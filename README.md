# Crear repositorio para git mediante comandos:

Primero me he dirigido la la carpeta principal mediante `cd..`, posteriormente me he ubicado en la carpeta D mediante el comando `cd d`. <br>
Una vez en el disco D, creamos un directorio llamado repositorioloc `mkdir repositorioloc` y mediante un `cd repositorioloc` nos situamos en el. <br>
Usamos un `git init` para iniciar el repositorio local. <br>
Asociamos nuestro gmail al projecto de git usando `git config user.email juanriudala@estudiante.edib.es` y posteriormente añadimos el nombre de usuario `git config user.name juanriudala` y asi nom hemos asociado a nuestra cuenta de gitlab.<br>
Asociamos la carpeta local a la carpeta remota `git remote add origin  https://bitbucket.org/lamadrederamon/joanriudalarepository.git` asi los tenemos asociados con la variable origin como vinculo.<br>
Vinculosmos al repositorio el archivo README.MD mediante el comando`git add README.MD`.
Mediante el comando `git commit -m "coment" README.MD` guardamos los canvios bajo un comentario que va entre las comillas, en nuestro caso "coment".<br>
Vamos al perfil de gitlab y sacamos un token, este nos dara un codigo de acceso al token.<br>
Hacemos un `git push origin master` en la consola, se abrira una pestañita donde en usuario usaras tu usuario de gitlab y contraseña el codigo de acceso al token, y la practica seria finalizada.<br>

<br><br>
COMANDOS:<br>
`cd..`  Ir al anterior repositorio.<br>
`cd d` Avanzar a dicho repositorio.<br>
`mkdir repositorioloc` Creamos repositorio.<br>
`cd repositorioloc` Avanzar a dicho repositorio.<br>
`git init` Inicia repositorio local.<br>
`git config user.email juanriudala@estudiante.edib.es` Asocia gmail.<br>
`git config user.name juanriudala` Asocia el usuario.<br>
`git remote add origin  https://bitbucket.org/lamadrederamon/joanriudalarepository.git` Asocia carpeta local a carpeta remota.<br>
`git add README.MD` Vinculosmos la carpeta al repositorio.<br>
`git commit -m "coment" README.MD` Guardar y actualizar cambios.<br>
`git push origin master` Indicas el link y la rama donde quieres subir el archivo.
<br><br>
[repositorioloc](https://bitbucket.org/lamadrederamon/joanriudalarepository)